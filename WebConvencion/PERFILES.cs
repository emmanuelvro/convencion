//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebConvencion
{
    using System;
    using System.Collections.Generic;
    
    public partial class PERFILES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PERFILES()
        {
            this.USUARIOS2 = new HashSet<USUARIOS>();
        }
    
        public int ID_PERFILES { get; set; }
        public string DESCRIPCION { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public Nullable<int> ID_USUARIOS { get; set; }
        public Nullable<System.DateTime> FECHA_MODIFICACION { get; set; }
        public Nullable<int> ID_USUARIOS_MODIFICACION { get; set; }
        public Nullable<int> READ_ONLY { get; set; }
        public Nullable<int> HIDDEN { get; set; }
        public Nullable<int> DELETED { get; set; }
    
        public virtual ESTADO_LOGICO ESTADO_LOGICO { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO1 { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO2 { get; set; }
        public virtual USUARIOS USUARIOS { get; set; }
        public virtual USUARIOS USUARIOS1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USUARIOS> USUARIOS2 { get; set; }
    }
}
