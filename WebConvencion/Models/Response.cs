﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebConvencion.Models
{
    public class Response
    {
        public bool isCorrect { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public int Code { get; set; }
        public bool Login { get; set; }

        public Response()
        {
            this.Login = false;
        }
    }
}