﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using WebConvencion.Models;

namespace WebConvencion.Controllers
{
    public class ConvencionController : Controller
    {
        // GET: Convencion
        private ConvencionEntities entidad = new ConvencionEntities();
        private Convencion conv = new Convencion();
        public ActionResult Index()
        {

            using (this.entidad = new ConvencionEntities())
            {
                foreach (var item in this.entidad.TIPO_TELEFONOS)
                {
                    this.conv.TiposTelefono.Add(new SelectListItem()
                    {
                        Text = item.DESCRIPCION,
                        Value = item.ID_TIPO_TELEFONOS.ToString()
                    });
                }

                foreach (var item in this.entidad.ESTADOS)
                {
                    this.conv.Estados.Add(new SelectListItem()
                    {
                        Text = item.DESCRIPCION,
                        Value = item.ID_ESTADOS.ToString()
                    });
                }


                foreach (var item in this.entidad.COORDINACIONES)
                {
                    this.conv.Coordinaciones.Add(new SelectListItem()
                    {
                        Text = item.DESCRIPCION,
                        Value = item.ID_COORDINACIONES.ToString()
                    });
                }

                foreach (var item in this.entidad.MESAS_TRABAJO)
                {
                    this.conv.MesasTrabajo.Add(new SelectListItem()
                    {
                        Text = item.DESCRIPCION,
                        Value = item.ID_MESAS_TRABAJO.ToString()
                    });
                }

                this.conv.TallaSudadera.Add(new SelectListItem()
                {
                    Text = "Chica",
                    Value = "Chica"
                });

                this.conv.TallaSudadera.Add(new SelectListItem()
                {
                    Text = "Mediana",
                    Value = "Mediana"
                });

                this.conv.TallaSudadera.Add(new SelectListItem()
                {
                    Text = "Grande",
                    Value = "Grande"
                });

                this.conv.TallaSudadera.Add(new SelectListItem()
                {
                    Text = "Extra grande",
                    Value = "Extra grande"
                });
            }
            return View(this.conv);
        }

        [HttpPost]
        public JsonResult Index(Convencion conv)
        {
            Response respuesta = new Response();

            if (ModelState.IsValid)
            {
                try
                {
                using (this.entidad = new ConvencionEntities())
                {

                    TELEFONOS telefono = new TELEFONOS()
                    {
                        ID_TELEFONOS = conv.IdTipoTelefono,
                        TELEFONO = conv.Telefono,
                        EXTENSION = conv.Extension,
                        COMENTARIO = conv.Comentario
                    };
                    USUARIOS user = new USUARIOS()
                    {
                        EMAIL = conv.Email,
                        CURP = conv.Curp,
                        A_PATERNO = conv.ApPaterno,
                        A_MATERNO = conv.ApMaterno,
                        NOMBRE = conv.Nombre,
                        FECHA_NACIMIENTO = conv.FechaNacimiento,
                        FECHA = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                        ID_ESTADOS = conv.idEstado,
                        ID_DELEGACIONES = conv.idDelegacion,
                        ID_COORDINACIONES = conv.idCoordinacionNacional,
                        ID_MESAS_TRABAJO = conv.idMesaTrabajo,
                        ASOCIADO_NUMERO = conv.NumeroAsociado,
                        FECHA_MODIFICACION = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                        TELEFONOS = telefono,
                        ID_TELEFONOS = telefono.ID_TELEFONOS,
                        TALLA_SUDADERA = conv.Talla
                       
                    };

                    this.entidad.USUARIOS.Add(user);
                    this.entidad.SaveChanges();
                        respuesta.Code = -100;
                        respuesta.Exception = null;
                        respuesta.isCorrect = true;
                        respuesta.Message = "Se registro correctamente";
                }
               }
                catch (EntityException ex)
                {

                    respuesta.Code = ex.GetHashCode();
                    respuesta.Exception = ex.Message;
                    respuesta.isCorrect = false;
                    respuesta.Message = "Ocurrio un error al Guardar";
                }
            }
            else
            {
                respuesta.Code = 500;
                respuesta.Exception = null;
                respuesta.isCorrect = false;
                respuesta.Message = "Ocurrio un error al Guardar, error en la información ingresada";
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDelegaciones(int id)
        {
            this.entidad = new ConvencionEntities();

            var del = from d in this.entidad.DELEGACIONES
                      where d.ID_ESTADOS == id
                      select d;
            foreach (var item in del)
            {
                this.conv.Delegaciones.Add(new SelectListItem()
                {
                    Text = item.DESCRIPCION,
                    Value = item.ID_DELEGACIONES.ToString()
                });
            }

            return Json(JsonConvert.SerializeObject(this.conv.Delegaciones), JsonRequestBehavior.AllowGet);
        }


        public JsonResult getAsociado(int numero)
        {
            Asociado asoc = new Asociado();
            XmlDocument xmldoc = new XmlDocument();

            //Load the XML file "xmlUrl" in the XMLDocument
            try
            {

            
            xmldoc.Load("https://sccrm.mx/classes/scClass.php?getByNumber=" + numero);
            XmlNode StuInfoNode = xmldoc.SelectSingleNode("data");
            asoc.Status = StuInfoNode.SelectSingleNode("status").InnerText == "TRUE" ? true : false;
            if (asoc.Status)
            {
                asoc.Nombre = StuInfoNode.SelectSingleNode("name").InnerText;
                asoc.ApellidoPaterno = StuInfoNode.SelectSingleNode("lastName").InnerText;
                asoc.ApellidoMaterno = StuInfoNode.SelectSingleNode("momLastName").InnerText;
                asoc.Email = StuInfoNode.SelectSingleNode("email").InnerText;
                asoc.Curp = StuInfoNode.SelectSingleNode("curp").InnerText;
                asoc.IdSexo = Convert.ToInt32(StuInfoNode.SelectSingleNode("sexID").InnerText);
                asoc.FechaNacimiento = Convert.ToDateTime(StuInfoNode.SelectSingleNode("birthDate").InnerText).ToString("dd-MM-yyyy");
                asoc.idTelefono = Convert.ToInt32(StuInfoNode.SelectSingleNode("phoneKindID").InnerText);
                asoc.Telefono = StuInfoNode.SelectSingleNode("phoneNumber").InnerText;
                asoc.Extension = string.IsNullOrEmpty(StuInfoNode.SelectSingleNode("phoneExtention").InnerText) ? 0 : Convert.ToInt32(StuInfoNode.SelectSingleNode("phoneExtention").InnerText);
                asoc.ComentariosTelefono = StuInfoNode.SelectSingleNode("phoneComment").InnerText;
                asoc.TipoTelefono = StuInfoNode.SelectSingleNode("phoneKind").InnerText;
                asoc.IdAsociado = Convert.ToInt32(StuInfoNode.SelectSingleNode("associateID").InnerText);
                asoc.IdDelegacon = Convert.ToInt32(StuInfoNode.SelectSingleNode("delegationID").InnerText);
                asoc.IdEstado = Convert.ToInt32(StuInfoNode.SelectSingleNode("stateID").InnerText);
                asoc.IdArea = Convert.ToInt32(StuInfoNode.SelectSingleNode("areaID").InnerText);
                asoc.NumeroAsociado = Convert.ToInt32(StuInfoNode.SelectSingleNode("associateNumber").InnerText);
                asoc.AsociadoTipo = StuInfoNode.SelectSingleNode("associate").InnerText;
                asoc.Delegacion = StuInfoNode.SelectSingleNode("delegation").InnerText;
                asoc.Estado = StuInfoNode.SelectSingleNode("state").InnerText;
                asoc.Area = StuInfoNode.SelectSingleNode("area").InnerText;
            }
            }
            catch (Exception)
            {
                asoc.Status = false;
            }
            return Json(JsonConvert.SerializeObject(asoc), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Login(Convencion model, string returnUrl)
        {
            Response respuesta = new Response();
            try
            {
                // Verification.    
                
                    // Initialization.    

                    var loginInfo = (from user in this.entidad.ESTADOS_ACCESO
                                     where user.USER == model.Username && user.PASSWORD == model.Password
                                     select user).ToList();
                    // Verification.    
                    if (loginInfo != null && loginInfo.Count() > 0)
                    {
                        // Initialization.    
                        var logindetails = loginInfo.First();
                        // Login In.    
                        this.SignInUser(logindetails.USER, false);
                    // Info.    
                    respuesta.isCorrect = true;
                    respuesta.Message = "Acceso Correcto, Ingresando...";
                    respuesta.Login = true;
                    }
                    else
                    {
                    // Setting.  
                    ViewBag.Respuesta = "Usuario o Contraseña incorrecta";
                    respuesta.isCorrect = false;
                    respuesta.Message = "Usuario o Contraseña incorrecta";
                    respuesta.Login = false;
                    ModelState.AddModelError(string.Empty, "Invalid username or password.");
                    }
                }
            
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // If we got this far, something failed, redisplay form    
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            try
            {
                // Setting.    
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                // Sign Out.    
                authenticationManager.SignOut();
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
            // Info.    
            return this.RedirectToAction("Index", "Convencion");
        }

        private void SignInUser(string username, bool isPersistent)
        {
            // Initialization.    
            var claims = new List<Claim>();
            try
            {
                // Setting    
                claims.Add(new Claim(ClaimTypes.Name, username));
                var claimIdenties = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                // Sign In.    
                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, claimIdenties);
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            try
            {
                // Verification.    
                if (Url.IsLocalUrl(returnUrl))
                {
                    // Info.    
                    return this.Redirect(returnUrl);
                }
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
            // Info.    
            return this.RedirectToAction("Index", "Home");
        }
    }
}