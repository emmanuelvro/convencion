//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebConvencion
{
    using System;
    using System.Collections.Generic;
    
    public partial class USUARIOS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public USUARIOS()
        {
            this.DELEGACIONES = new HashSet<DELEGACIONES>();
            this.DELEGACIONES1 = new HashSet<DELEGACIONES>();
            this.PABELLONES_VISITAS = new HashSet<PABELLONES_VISITAS>();
            this.PERFILES = new HashSet<PERFILES>();
            this.PERFILES1 = new HashSet<PERFILES>();
            this.REL_PABELLONES_VISITAS_PABELLONES_ACTIVIDADES = new HashSet<REL_PABELLONES_VISITAS_PABELLONES_ACTIVIDADES>();
            this.USUARIOS1 = new HashSet<USUARIOS>();
            this.USUARIOS11 = new HashSet<USUARIOS>();
        }
    
        public int ID_USUARIOS { get; set; }
        public string CURP { get; set; }
        public string EMAIL { get; set; }
        public string PASSWD { get; set; }
        public string A_PATERNO { get; set; }
        public string A_MATERNO { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<System.DateTime> FECHA_NACIMIENTO { get; set; }
        public string ASOCIADO_NUMERO { get; set; }
        public Nullable<int> ID_PERFILES { get; set; }
        public Nullable<int> ID_ESTADOS { get; set; }
        public Nullable<int> ID_DELEGACIONES { get; set; }
        public Nullable<int> ID_ACTIVA { get; set; }
        public Nullable<int> ID_COORDINACIONES { get; set; }
        public Nullable<int> ID_ASOCIADOS_TIPOS { get; set; }
        public Nullable<int> ID_TELEFONOS { get; set; }
        public string ALERGIAS { get; set; }
        public string ACCIDENTE_AVISAR { get; set; }
        public Nullable<int> ACCIDENTE_TELEFONOS { get; set; }
        public string REFERENCIA_PAGO { get; set; }
        public Nullable<int> ID_ESTATUS { get; set; }
        public Nullable<int> ID_MESAS_TRABAJO { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public Nullable<int> ID_USUARIOS_REGISTRO { get; set; }
        public Nullable<System.DateTime> FECHA_MODIFICACION { get; set; }
        public Nullable<int> ID_USUARIOS_MODIFICACION { get; set; }
        public Nullable<int> READ_ONLY { get; set; }
        public Nullable<int> HIDDEN { get; set; }
        public Nullable<int> DELETED { get; set; }
        public string TALLA_SUDADERA { get; set; }
    
        public virtual ASOCIADOS_TIPOS ASOCIADOS_TIPOS { get; set; }
        public virtual COORDINACIONES COORDINACIONES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DELEGACIONES> DELEGACIONES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DELEGACIONES> DELEGACIONES1 { get; set; }
        public virtual DELEGACIONES DELEGACIONES2 { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO1 { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO2 { get; set; }
        public virtual ESTADO_LOGICO ESTADO_LOGICO3 { get; set; }
        public virtual ESTADOS ESTADOS { get; set; }
        public virtual ESTATUS ESTATUS { get; set; }
        public virtual MESAS_TRABAJO MESAS_TRABAJO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PABELLONES_VISITAS> PABELLONES_VISITAS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PERFILES> PERFILES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PERFILES> PERFILES1 { get; set; }
        public virtual PERFILES PERFILES2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REL_PABELLONES_VISITAS_PABELLONES_ACTIVIDADES> REL_PABELLONES_VISITAS_PABELLONES_ACTIVIDADES { get; set; }
        public virtual TELEFONOS TELEFONOS { get; set; }
        public virtual TELEFONOS TELEFONOS1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USUARIOS> USUARIOS1 { get; set; }
        public virtual USUARIOS USUARIOS2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USUARIOS> USUARIOS11 { get; set; }
        public virtual USUARIOS USUARIOS3 { get; set; }
    }
}
