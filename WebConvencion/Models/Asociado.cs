﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebConvencion.Models
{
    public class Asociado
    {
        public bool Status { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public string Curp { get; set; }
        public int IdSexo { get; set; }
        public string FechaNacimiento { get; set; }
        public int idTelefono { get; set; }
        public string Telefono { get; set; }
        public int Extension { get; set; }
        public string ComentariosTelefono { get; set; }
        public string TipoTelefono { get; set; }
        public int IdAsociado { get; set; }
        public int IdDelegacon { get; set; }
        public int IdEstado { get; set; }
        public int IdArea { get; set; }
        public int NumeroAsociado { get; set; }
        public string AsociadoTipo { get; set; }
        public string Delegacion { get; set; }
        public string Estado { get; set; }
        public string Area { get; set; }
    }
}