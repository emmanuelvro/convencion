﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebConvencion.Models
{
    public class Convencion
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("CURP")]
        public string Curp { get; set; }
        [Required]
        [DisplayName("Apellido Paterno")]
        public string ApPaterno { get; set; }
        [Required]
        [DisplayName("Apellido Materno")]
        public string ApMaterno { get; set; }
        [Required]
        [DisplayName("Nombre")]
        public string Nombre { get; set; }
        [Required]
        [DisplayName("Fecha Nacimiento")]
        public DateTime FechaNacimiento { get; set; }
        [DisplayName("Tipo Telefono")]
        public int IdTipoTelefono { get; set; }
        [Required]
        [DisplayName("Telefono")]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }
        [Required]
        [DisplayName("Extension")]
        public int Extension { get; set; }
        [DisplayName("Comentario")]
        [DataType(DataType.MultilineText)]
        public string Comentario { get; set; }
        [DisplayName("Estado")]
        public int idEstado { get; set; }
        [DisplayName("Delegacion")]
        public int idDelegacion { get; set; }
        [DisplayName("Coordinacion Nacional")]
        public int idCoordinacionNacional { get; set; }
        [DisplayName("Mesa de Trabajo")]
        public int idMesaTrabajo { get; set; }
        [DisplayName("Numero Asociado")]
        public string NumeroAsociado { get; set; }
        [DisplayName("Talla Sudadera")]
        public string Talla { get; set; }
        public List<SelectListItem> TiposTelefono { get; set; }
        public List<SelectListItem> Estados { get; set; }
        public List<SelectListItem> Delegaciones { get; set; }
        public List<SelectListItem> Coordinaciones { get; set; }
        public List<SelectListItem> MesasTrabajo { get; set; }
        public List<SelectListItem> TallaSudadera { get; set; }
     
        [Display(Name = "Usuario")]
        public string Username
        {
            get;
            set;
        }
   
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get;
            set;
        }

        public Convencion()
        {
            this.TiposTelefono = new List<SelectListItem>();
            this.Estados = new List<SelectListItem>();
            this.Delegaciones = new List<SelectListItem>();
            this.Coordinaciones = new List<SelectListItem>();
            this.MesasTrabajo = new List<SelectListItem>();
            this.TallaSudadera = new List<SelectListItem>();
            this.Username = string.Empty;
            this.Password = string.Empty;
        }
    }
}