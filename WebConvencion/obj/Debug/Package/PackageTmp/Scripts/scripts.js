﻿$(document).ready(function () {

    $("#estado").change(function () {
        var idEstado = $(this).val();
        cargaDelegacion(idEstado);

    });


    $('#asociado').keypress(function (e) {
        var keyCode = e.keyCode || e.which;
        if (e.keyCode == 13 || keyCode === 9) {
            var idAsociado = $(this).val();

            limpiarForm();

            var url = "/Convencion/getAsociado/?numero=" + idAsociado;
            $.get(url)
            .done(function (data) {
                var datos = $.parseJSON(data);
                
                if (datos.Status) {
                    $("#mail").val(datos.Email);
                    $("#mail").prop("readonly", true);

                    $("#Curp").val(datos.Curp);
                    $("#Curp").prop("readonly", true);

                    $("#ApPaterno").val(datos.ApellidoPaterno);
                    $("#ApPaterno").prop("readonly", true);

                    $("#ApMaterno").val(datos.ApellidoMaterno);
                    $("#ApMaterno").prop("readonly", true);

                    $("#Nombre").val(datos.Nombre);
                    $("#Nombre").prop("readonly", true);

                    $("#FechaNacimiento").val(datos.FechaNacimiento);

                    $('#IdTipoTelefono option:contains("' + datos.TipoTelefono + '")').prop('selected', true);

                    $("#Telefono").val(datos.Telefono);
                    $("#Telefono").prop("readonly", true);

                    $("#Extension").val(datos.Extension);
                    $("#Extension").prop("readonly", true);

                    $("#Comentario").val(datos.ComentariosTelefono);
                    $("#Comentario").prop("readonly", true);

                    $('#estado option:contains("' + datos.Estado + '")').prop('selected', true);
                    cargaDelegacion($('#estado').val());
                    $('#delegacion option').filter(function () {
                        return ($(this).val() == datos.IdDelegacon); //To select Blue
                    }).prop('selected', true);
                } else {
                    alerta("No se encontro el numero de asociado, capture la información", true);
                }
                console.log(datos);
               
            })
            .fail(function (data) {
                console.log(data.error);
            });
            return false; // prevent the button click from happening
        }
    });


    $('#asociado').blur(function (e) {
            var idAsociado = $(this).val();

            limpiarForm();

            var url = "/Convencion/getAsociado/?numero=" + idAsociado;
            $.get(url)
            .done(function (data) {
                var datos = $.parseJSON(data);

                if (datos.Status) {
                    $("#mail").val(datos.Email);
                    $("#mail").prop("readonly", true);

                    $("#Curp").val(datos.Curp);
                    $("#Curp").prop("readonly", true);

                    $("#ApPaterno").val(datos.ApellidoPaterno);
                    $("#ApPaterno").prop("readonly", true);

                    $("#ApMaterno").val(datos.ApellidoMaterno);
                    $("#ApMaterno").prop("readonly", true);

                    $("#Nombre").val(datos.Nombre);
                    $("#Nombre").prop("readonly", true);

                    $("#FechaNacimiento").val(datos.FechaNacimiento);

                    $('#IdTipoTelefono option:contains("' + datos.TipoTelefono + '")').prop('selected', true);

                    $("#Telefono").val(datos.Telefono);
                    $("#Telefono").prop("readonly", true);

                    $("#Extension").val(datos.Extension);
                    $("#Extension").prop("readonly", true);

                    $("#Comentario").val(datos.ComentariosTelefono);
                    $("#Comentario").prop("readonly", true);

                    $('#estado option:contains("' + datos.Estado + '")').prop('selected', true);
                    cargaDelegacion($('#estado').val());
                    $('#delegacion option').filter(function () {
                        return ($(this).val() == datos.IdDelegacon); //To select Blue
                    }).prop('selected', true);
                } else {
                    alerta("No se encontro el numero de asociado, capture la información", true);
                }
                console.log(datos);

            })
            .fail(function (data) {
                console.log(data.error);
            });
            return false; // prevent the button click from happening
        
    });


    $("form").submit(function (e) {
        $.post($(this).attr("action"),
                $(this).serialize(),
                function (data) {
                    console.log(data);
                    //respuesta = JSON.parse(data);
                    if ($.trim(data.isCorrect) == "true") {
                        alerta(data.Message, false);
                        $("form")[0].reset();
                        if (data.Login) {
                            window.location = "/";
                        } else {
                            limpiarForm();
                        }
                        
                    } else {
                        alerta(data.Message, true);
                    }
                   // $("#result").html(data);
                    
                    
                });
        e.preventDefault();
        return false;
    });

    $.validator.addMethod('date',
    function (value, element) {
        if (this.optional(element)) {
            return true;
        }
        var ok = true;
        try {
            $.datepicker.parseDate('dd-mm-yy', value);
        }
        catch (err) {
            ok = false;
        }
        return ok;
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(".date").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true, yearRange: "1940:2017" });
    
   
});

function alerta(data, error) {
    if (!error) {
        tipo = "success";
        icono = 'glyphicon glyphicon-ok';
    } else {
        tipo = "danger";
        icono = 'glyphicon glyphicon-remove';
    }
    $.notify({
        // options
        icon: icono,
        //title: 'Alerta',
        message: data,
    }, {
        // settings
        element: 'body',
        position: null,
        type: tipo,
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "center"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}


$(function () {
    //all the menu items
    var $items = $('#cc_menu .cc_item');
    //number of menu items
    var cnt_items = $items.length;
    //if menu is expanded then folded is true
    var folded = false;
    //timeout to trigger the mouseenter event on the menu items
    var menu_time;
    /**
    bind the mouseenter, mouseleave to each item:
    - shows / hides image and submenu
    bind the click event to the list elements (submenu):
    - hides all items except the clicked one, 
    and shows the content for that item
    */
    $items.unbind('mouseenter')
          .bind('mouseenter', m_enter)
          .unbind('mouseleave')
          .bind('mouseleave', m_leave)
          .find('.cc_submenu > ul > li')
          .bind('click', function () {
              var $li_e = $(this);
              //if the menu is already folded,
              //just replace the content
              if (folded) {
                  hideContent();
                  showContent($li_e.attr('class'));
              }
              else //fold and show the content
                  fold($li_e);
          });

    /**
    mouseenter function for the items
    the timeout is used to prevent this event 
    to trigger if the user moves the mouse with 
    a considerable speed through the menu items
    */
    function m_enter() {
        var $this = $(this);
        clearTimeout(menu_time);
        menu_time = setTimeout(function () {
            //img
            $this.find('img').stop().animate({ 'top': '0px' }, 400);
            //cc_submenu ul
            $this.find('.cc_submenu > ul').stop().animate({ 'height': '200px' }, 400);
        }, 200);
    }

    //mouseleave function for the items
    function m_leave() {
        var $this = $(this);
        clearTimeout(menu_time);
        //img
        $this.find('img').stop().animate({ 'top': '-600px' }, 400);
        //cc_submenu ul
        $this.find('.cc_submenu > ul').stop().animate({ 'height': '0px' }, 400);
    }

    //back to menu button - unfolds the menu
    $('#cc_back').bind('click', unfold);

    /**
    hides all the menu items except the clicked one
    after that, the content is shown
    */
    function fold($li_e) {
        var $item = $li_e.closest('.cc_item');

        var d = 100;
        var step = 0;
        $items.unbind('mouseenter mouseleave');
        $items.not($item).each(function () {
            var $item = $(this);
            $item.stop().animate({
                'marginLeft': '-140px'
            }, d += 200, function () {
                ++step;
                if (step == cnt_items - 1) {
                    folded = true;
                    showContent($li_e.attr('class'));
                }
            });
        });
    }

    /**
    shows all the menu items 
    also hides any item's image / submenu 
    that might be displayed
    */
    function unfold() {
        $('#cc_content').stop().animate({ 'left': '-1000px' }, 600, function () {
            var d = 100;
            var step = 0;
            $items.each(function () {
                var $item = $(this);

                $item.find('img')
                     .stop()
                     .animate({ 'top': '-600px' }, 200)
                     .andSelf()
                     .find('.cc_submenu > ul')
                     .stop()
                     .animate({ 'height': '0px' }, 200);

                $item.stop().animate({
                    'marginLeft': '0px'
                }, d += 200, function () {
                    ++step;
                    if (step == cnt_items - 1) {
                        folded = false;
                        $items.unbind('mouseenter')
                              .bind('mouseenter', m_enter)
                              .unbind('mouseleave')
                              .bind('mouseleave', m_leave);

                        hideContent();
                    }
                });
            });
        });
    }

    //function to show the content
    function showContent(idx) {
        $('#cc_content').stop().animate({ 'left': '420px' }, 200, function () {
            $(this).find('.' + idx).fadeIn();
            if (idx == "cc_content_1")
                $("#prueba").show();
            else
                $("#prueba").hide();
            console.log(idx);
        });
    }

    //function to hide the content
    function hideContent() {
        $('#cc_content').find('div').hide();
    }
});


function cargaDelegacion(idEstado) {
    var url = "/Convencion/getDelegaciones/" + idEstado;
    $.get(url)
    .done(function (data) {
        $("#delegacion").empty();
        $.each(JSON.parse(data), function (i, row) {
            var $option = $('<option>');
            $option.val(row.Value);
            $option.html(row.Text);
            $("#delegacion").append($option);
        })
    })
    .fail(function (data) {
        console.log(data.error);
    });
}

function limpiarForm() {

    $("#mail").val('');
    $("#mail").prop("readonly", false);

    $("#Curp").val('');
    $("#Curp").prop("readonly", false);

    $("#ApPaterno").val('');
    $("#ApPaterno").prop("readonly", false);

    $("#ApMaterno").val('');
    $("#ApMaterno").prop("readonly", false);

    $("#Nombre").val('');
    $("#Nombre").prop("readonly", false);

    $("#FechaNacimiento").val('');

    $('#IdTipoTelefono option:contains("Celular")').prop('selected', true);

    $("#Telefono").val('');
    $("#Telefono").prop("readonly", false);

    $("#Extension").val('');
    $("#Extension").prop("readonly", false);

    $("#Comentario").val('');
    $("#Comentario").prop("readonly", false);
}